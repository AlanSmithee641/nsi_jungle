---
hide:
  - toc
---

# Ressources externes

## Spécialité NSI

## Plateformes d'apprentissage

<table>
    <tr>
        <th>Nom</th>
        <th>Format</th>
        <th>Thèmes</th>
        <th>Public</th>
        <th>Langues</th>
        <th>Adéquation lycée</th>
    </tr>
    <tr>
        <td><a href="https://www.france-ioi.org/algo/chapters.php">France-IOI</a></td>
        <td>Cours, exercices</td>
        <td>Algo, programmation</td>
        <td>8+</td>
        <td>🇫🇷 🇬🇧 🇪🇸 🇱🇹</td>
        <td>⭐⭐⭐</td>
    </tr>
    <tr>
        <td><a href="https://projecteuler.net/">Project Euler</a></td>
        <td>Exercices</td>
        <td>Algo, programmation</td>
        <td>10+</td>
        <td>🇬🇧</td>
        <td>⭐⭐</td>
    </tr>
</table>


## Chaînes Youtube
