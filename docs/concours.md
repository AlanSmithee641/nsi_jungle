Les élèves de lycée ont de plus en plus d'opportunités
de passer des concours touchant aux différents domaines de l'informatique.
Certaines sessions de passage seront organisées directement en classe.
Pour les autre concours, c'est aux élèves de réaliser les démarches nécessaires pour y participer.

Le tableau ci-dessous répertorie les différents concours accessibles
au cours de l'année scolaire 2024/2025, classés par date.



#### Castor informatique
* Passage en classe entre le 0/11/2024 et le 21/12/2024.
* Seul ou en binome.
* Permet de se qualifier au concours Algoréa.
* Catégorie en fonction du score atteint.
* [Page du concours](https://castor-informatique.fr/)

#### Hack ton lycée
* Réservé aux lycées et enseignants franciliens.
* Inscription jusqu'au 10/10/2024.
* Phase 1 (Capture the Flag) : 18/10/241 &rarr; 19/10/24
* Phase 2 (Bug Bounty) : 25/12/24 &rarr; 31/10/24
* [Page du concours](https://transfonum.monlycee.net/hack-ton-lycee/),
  [plus d'informations](https://transfonum.monlycee.net/wp-content/uploads/2024/10/Hack-Ton-Lycee-Webinaire-de-lancement-10-10-2024.pdf)
  
#### Al Kindi

#### Passe ton hack d'abord