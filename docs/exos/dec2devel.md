# Trouver l'écriture dans une base donnée

Quelle est l'écriture du nombre <span id="x"></span> en base <span id="base"></span> ?

<input type="text" id="rep">
<button id="verif">Vérifier</button>
<button id="regen">Recommencer</button>


<script src="../../javascripts/dec2devel.js" defer></script>