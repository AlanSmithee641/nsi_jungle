# Complément à 2 : trouver la représentation

Trouver la représentation en complément à deux sur un octet du nombre <code id="x"></code>.
On saisira dans le champ ci-dessous la réponse au format <code>0b...</code>

<input type="text" id="rep">
<button id="verif">Vérifier</button>
<button id="regen">Recommencer</button>


<script src="../../javascripts/cp2_n2dev.js" defer></script>