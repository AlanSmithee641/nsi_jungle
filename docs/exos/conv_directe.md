# Convertir directement dans une autre base

Déterminer l'écriture de <span class="devel" id="devel"></span><sub id="b1"></sub>
en base <span id="b2"></span>.

<input type="text" id="rep">
<button id="verif">Vérifier</button>
<button id="regen">Recommencer</button>

<script src="../../javascripts/conv_directe.js" defer></script>