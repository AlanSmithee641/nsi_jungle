# Complément à 2 : trouver la valeur

Quelle est la valeur du nombre représenté par <code id="devel"></code> en complément à deux ?

<input type="text" id="rep">
<button id="verif">Vérifier</button>
<button id="regen">Recommencer</button>


<script src="../../javascripts/cp2_dev2n.js" defer></script>