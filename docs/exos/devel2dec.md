# Trouver l'écriture décimale


Que vaut le nombre <span class="devel" id="devel"></span><sub class="base" id="base"></sub> ?

<input type="text" id="rep">
<button id="verif">Vérifier</button>
<button id="regen">Recommencer</button>


<script src="../../javascripts/devel2dec.js" defer></script>