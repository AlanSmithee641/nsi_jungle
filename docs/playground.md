Sur cette page non répertoriée on trouvera des exemples minimaux illustrant
les possiblités de `mkdocs`.

```python title="Lancer de dé"
from random import randint

# un commentaire parce que

def de():
	while True:
		return 42 + randint(1, 60)
	print("ça n'arrivera jamais")
```

Mon emoji :surfer: fonctionne-t-il ?

<table>
  <tr>
    <th>titre</th>
    <th>titre</th>
    <th>titre</th>
  </tr>
  <tr>
    <th>titre</th>
    <td>valeur</td>
    <td>valeur</td>
  </tr>
  <tr>
    <th>titre</th>
    <td>valeur</td>
    <td>valeur</td>
  </tr>
</table>

Lorem ipsum[^1] dolor sit amet, consectetur adipiscing elit.[^2]

[^1]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
[^2]:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

!!! note "Titre du bloc"
    You should note that the title will be automatically capitalized.

!!! danger "Fais gaffe mon gars !"
    proute

??? important
    J'ai oublié.

Un exemple de `code` en ligne.


``` mermaid
graph LR
  A[Start] --> B{Error?};
  B -->|Yes| C[Hmm...];
  C --> D[Debug];
  D --> B;
  B ---->|No| E[Yay!];
```

<style>
table.simple{
	border: none;
}
</style>

<table class="simple">
	<tr style="border: none;">
		<th style="border: none;"></th>
		<th>b</th>
		<th>c</th>
	</tr>
	<tr>
		<th>a</th>
		<td>b</td>
		<td>c</td>
	</tr>
	<tr>
		<th>a</th>
		<td>b</td>
		<td>c</td>
	</tr>
</table>+

# Quae aequoris quod animis ex feracis senectae

$$
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
$$

\[
\operatorname{ker} f=\{g\in G:f(g)=e_{H}\}{\mbox{.}}
\]

The homomorphism $f$ is injective if and only if its kernel is only the 
singleton set $e_G$, because otherwise $\exists a,b\in G$ with $a\neq b$ such 
that $f(a)=f(b)$.


++ctrl+alt+f5++

<mark>surligné</mark>

Un mot <ins>très</ins> sournoisement inséré.
