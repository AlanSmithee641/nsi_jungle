## Polycopiés

- Bases de numération : 
  [cours](./polys/numeration_cours.pdf), 
  [exercices](./polys/numeration_exos.pdf),
  [corrigés](./polys/numeration_corriges.pdf),
  [projection](./polys/proj_base.pdf)
<!--
- Notion d'information : 
  [cours](./polys/information_cours.pdf),
  [exercices](./polys/information_exos.pdf),
  [corrigés](./polys/information_corriges.pdf)
- Représentation des entiers signés :
  [cours](./polys/cp2_cours.pdf), 
  [exercices](./polys/cp2_exos.pdf)
- Expressions booléennes :
  [cours](./polys/boole_cours.pdf), 
  [exercices](./polys/boole_exos.pdf)
- HTML/CSS :
  [cours](./polys/html_css_cours.pdf), 
  [exercices](./polys/html_css_exos.pdf),
  [archive](./polys/html_css.zip)
- Représentation du texte :
  [cours](./polys/encodage_cours.pdf), 
  [exercices](./polys/encodage_exos.pdf)
- JavaScript :
  [cours](./polys/js_cours.pdf),
  [exercices](./polys/js_exos.pdf),
  [archive](./polys/js_fichiers.zip),
  [corrigés](./polys/js_corriges.zip)
- Tris :
  [cours](./polys/tris_cours.pdf),
  [exercices](./polys/tris_exos.pdf),
  [archive](./polys/tris_sources.zip),
  [corrigés](./polys/tris_corriges.zip)
- Gestion des fichiers sous Python :
  [cours](./polys/fichiers_cours.pdf), 
  [exercices](./polys/fichiers_exos.pdf),
  [archive](./polys/fichiers_archive.zip)
<!--
- Réseaux logiques
- Portes logiques
- Architecture de Von Neumann
-->

<!--
## Activités Capytale

- [Mon premier script](https://capytale2.ac-paris.fr/web/c/869a-1795254)
- Rudiments de Python :
  [cours](https://capytale2.ac-paris.fr/web/c/bd7f-1795284),
  [exercices](https://capytale2.ac-paris.fr/web/c/2cc3-1795359),
  [corrigés](https://capytale2.ac-paris.fr/web/c/2acd-1842423)
- Boucles bornées :
  [cours](https://capytale2.ac-paris.fr/web/c/3232-1842450),
  [exercices](https://capytale2.ac-paris.fr/web/c/ebda-1842479),
  [corrigés](https://capytale2.ac-paris.fr/web/c/7fac-1872372)
- Embranchements :
  [cours](https://capytale2.ac-paris.fr/web/c/0bb1-1872414),
  [exercices](https://capytale2.ac-paris.fr/web/c/9409-1872429)
-->