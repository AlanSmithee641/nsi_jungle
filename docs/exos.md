---
title: Exos interactifs
hide: toc
---

## Numération
- [Trouver l'écriture décimale](./exos/devel2dec.md)
- [Trouver l'écriture dans une base donnée](./exos/dec2devel.md)
- [Conversion directe](./exos/conv_directe.md)

## Complément à deux
- [Trouver la valeur représentée](./exos/cp2_dev2n.md)
- [Trouver la représentation](./exos/cp2_n2dev.md)

## Réseau