function regen(){
	x = entierAlea(0, 300);
	var couples = [[2, 4], [2, 8], [2, 16], [4, 2], [4, 16], [8, 2], [16, 2], [16, 4]];
	var couple = couples[entierAlea(0, 7)];
	b1 = couple[0];
	b2 = couple[1];
	devel = decomp(x, b1);
	champ_devel.innerHTML = devel;
	champ_b1.innerHTML = b1;
	champ_b2.innerHTML = b2;
	champ_rep.value = "";
}

function verif(){
	var ch_rep = champ_rep.value;
	if (estValide(ch_rep, b2)){
		if (parseInt(ch_rep, b2)==x){
			succes();
		}
		else{
			echec();
		}
	}
}

// constantes numériques globales
var x;
var devel;

// éléments <span>
var champ_devel = document.querySelector("#devel");
var champ_b1 = document.querySelector("#b1");
var champ_b2 = document.querySelector("#b2");

// champs réponse
var champ_rep = document.querySelector("#rep");

// boutons
var bouton_verif = document.querySelector("#verif");
var bouton_regen = document.querySelector("#regen");

regen();
bouton_verif.addEventListener("click", verif);
bouton_regen.addEventListener("click", regen);
