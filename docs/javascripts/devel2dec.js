function regen(){
	base = entierAlea(2, 16);
	x = entierAlea(0, 300);
	devel = decomp(x, base);
	champ_devel.innerHTML = devel;
	champ_base.innerHTML = base;
	champ_rep.value = "";
}


function verif(){
	var ch_rep = champ_rep.value;
	var int_rep = parseInt(ch_rep, 10);
	if (int_rep==x){
		succes();		
	}
	else{
		echec();
	}
}


// constantes numériques globales
var x;
var devel;
var base;

// éléments <span>
var champ_devel = document.querySelector("#devel");
var champ_base = document.querySelector("#base");

// champs réponse
var champ_rep = document.querySelector("#rep");

// boutons
var bouton_verif = document.querySelector("#verif");
var bouton_regen = document.querySelector("#regen");


regen();
bouton_verif.addEventListener("click", verif);
bouton_regen.addEventListener("click", regen);