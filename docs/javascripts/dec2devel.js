function regen(){
	base = entierAlea(2, 16);
	x = entierAlea(0, 300);
	devel = decomp(x, base);
	champ_x.innerHTML = x;
	champ_base.innerHTML = base;
	champ_rep.value = "";
}


function verif(){
	var ch_rep = document.getElementById("rep").value;
	if (ch_rep.length==0){
		alert("Il faut saisir une réponse !");
	}
	else{
		var int_rep = parseInt(ch_rep, base);
		if (int_rep==x){
			succes();		
		}
		else{
			echec();
		}
	}
}



// constantes numériques globales
var x;
var devel;
var base;

// éléments <span>
var champ_x = document.querySelector("#x");
var champ_base = document.querySelector("#base");

// champs réponse
var champ_rep = document.querySelector("#rep");

// boutons
var bouton_verif = document.querySelector("#verif");
var bouton_regen = document.querySelector("#regen");

regen();
bouton_verif.addEventListener("click", verif);
bouton_regen.addEventListener("click", regen);