function regen(){
	x = entierAlea(-128, 127);
	devel = cp2_n2dev(x, 8);
	champ_devel.innerHTML = devel;
	champ_rep.value = "";
}


function verif(){
	var ch_rep = champ_rep.value;
	if (parseInt(ch_rep, 10)==x){
		succes();
	}
	else{
		echec();
	}
}

// constantes numériques globales
var x;
var devel;

// éléments <span>
var champ_devel = document.querySelector("#devel");

// champs réponse
var champ_rep = document.querySelector("#rep");

// boutons
var bouton_verif = document.querySelector("#verif");
var bouton_regen = document.querySelector("#regen");


regen();
bouton_verif.addEventListener("click", verif);
bouton_regen.addEventListener("click", regen);