function entierAlea(mini, maxi){
	// renvoie un entier choisi aléatoirement entre mini et maxi
	// selon une loi uniforme
	var n = Math.floor((maxi-mini+1)*Math.random()) + mini;
	return n;
}


function echec(){
	// affiche en pop-up un message d'échec
	var longueur_tab_echecs = tab_echecs.length;
	var index_echec = entierAlea(0, longueur_tab_echecs-1)
	alert(tab_echecs[index_echec]);
}

function succes(){
	// affiche en pop-up un message de succès
	var longueur_tab_succes = tab_succes.length;
	var index_succes = entierAlea(0, longueur_tab_succes-1)
	alert(tab_succes[index_succes]);
}


function estValide(ch, b){
	// vérifie si la chaîne ch est valide en base b<=36
	var long = ch.length;
	if (long==0){
		alert("Il faut saisir une réponse.");
		return false;
	}
	else{
		for (var i=0; i<long; i++){
			var car = ch[i];
			var valDansBase = parseInt(car, b);
			var valAttendue = parseInt(car, 36);
			if (valDansBase!=valAttendue){
				alert("L'expression " + ch + " est invalide en base " + b + ".");
				return false;
			}
		}
	}
	return true;
}


function decomp(n, b){
	// écriture de n en base b
	return n.toString(b);
}

function zeroFill(ch, n){
	// complète ch par des zéros à gauche jusque à n chiffres
	var long = ch.length;
	return "0".repeat(n-long) + ch;
}

function cp2_n2dev(n, nbBits){
	// nombre vers son écriture en complément à 2
	// n supposé dans le bon intervalle
	var prefixe = "0b";
	var dev;
	if (n>=0){
		dev = zeroFill(decomp(n, 2), nbBits);
	}
	else{
		dev = decomp(Math.pow(2, nbBits) + n, 2);
	}
	return prefixe + dev;
}

var tab_echecs = [
	"Ce n'est pas ça.",
	"Avez-vous bien cherché ?",
	"Vous en êtes sûr ?",
	"Je crois pas non.",
	"Même pas en rêve.",
	"Désolé, mais non.",
	"Bravo pour votre clic ! Mais ce n'est pas la bonne réponse.",
	"La pire erreur n'est pas dans l'échec mais dans l'incapacité de dominer l'échec.",
	"L'échec est le fondement de la réussite.",
	"👎",
	"መልስህ የተሳሳተ ነው። ህመምዎን ለማስታገስ አንዳንድ የአማርኛ ፊደላት ገፀ-ባህሪያት እዚህ አሉ።"
]

var tab_succes = [
	"Oui !",
	"Bravo !",
	"Pas mal.",
	"Enfin !",
	"👍"
]
