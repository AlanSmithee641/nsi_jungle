function regen(){
	x = entierAlea(-128, 127);
	champ_x.innerHTML = x;
	champ_rep.value = "";
}

function verif(){
	var ch_rep = champ_rep.value;
    if (ch_rep.length>1){
        if (ch_rep.substring(0, 2)=="0b"){
            if (ch_rep.length==10){
                var dev = cp2_n2dev(x, 8);
                if (ch_rep==dev){
                    succes();
                }
                else{
                    echec();
                }
            }
            else{
                alert("Le nombre doit être représenté sur un octet.");
            }
        }
        else{
            alert("Votre réponse doit commencer par 0b");
        }
    }
    else{
        alert("Votre réponse est bien trop courte !");
    }
}

// constantes numériques globales
var x;

// éléments <span>
var champ_x = document.querySelector("#x");

// champs réponse
var champ_rep = document.querySelector("#rep");

// boutons
var bouton_verif = document.querySelector("#verif");
var bouton_regen = document.querySelector("#regen");


regen();
bouton_verif.addEventListener("click", verif);
bouton_regen.addEventListener("click", regen);