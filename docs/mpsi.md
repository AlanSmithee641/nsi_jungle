## Polycopiés

- Bases de numération : 
  [cours](./polys/numeration_cours.pdf), 
  [exercices](./polys/numeration_exos.pdf),
  [corrigés](./polys/numeration_corriges.pdf),
  [projection](./polys/proj_base.pdf)
<!--
- Notion d'information : 
  [cours](./polys/information_cours.pdf),
  [exercices](./polys/information_exos.pdf),
  [corrigés](./polys/information_corriges.pdf)
- Représentation des entiers signés :
  [cours](./polys/cp2_cours.pdf), 
  [exercices](./polys/cp2_exos.pdf)
- Expressions booléennes :
  [cours](./polys/boole_cours.pdf), 
  [exercices](./polys/boole_exos.pdf)
- HTML/CSS :
  [cours](./polys/html_css_cours.pdf), 
  [exercices](./polys/html_css_exos.pdf),
  [archive](./polys/html_css.zip)
- Représentation du texte :
  [cours](./polys/encodage_cours.pdf), 
  [exercices](./polys/encodage_exos.pdf)
- JavaScript :
  [cours](./polys/js_cours.pdf),
  [exercices](./polys/js_exos.pdf),
  [archive](./polys/js_fichiers.zip),
  [corrigés](./polys/js_corriges.zip)
- Tris :
  [cours](./polys/tris_cours.pdf),
  [exercices](./polys/tris_exos.pdf),
  [archive](./polys/tris_sources.zip),
  [corrigés](./polys/tris_corriges.zip)
- Gestion des fichiers sous Python :
  [cours](./polys/fichiers_cours.pdf), 
  [exercices](./polys/fichiers_exos.pdf),
  [archive](./polys/fichiers_archive.zip)
- Réseaux logiques
- Portes logiques
- Architecture de Von Neumann
-->
  